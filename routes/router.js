const express = require('express');
const route = express.Router();
const services = require('../services/render'); 

    /*route.get('/', (req,res)=>{
        // res.send("crud Application");
        res.render('index');
    })*/
    /**
     * @description Root Route
     * @method GET
     */
    route.get('/', services.homeRoutes);



    /*route.get('/add-user',(req,res)=>{
        res.render('adduser');
    })*/
    /**
     * @description For add users
     * @method GET/add-user
     */
    route.get('/add-user', services.adduser)


    /*route.get('/update-user',(req,res)=>{
        res.render('updateuser');
    })*/
    /**
     * @description for Update user
     * @method GET/update-user
     */
    route.get('/update-user', services.updateuser)

    module.exports = route
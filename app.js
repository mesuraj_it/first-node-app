const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const bodyparser = require('body-parser');
const path = require('path');

const connectDB = require('./connection');

const app = express();

dotenv.config({path:'.env'});
const PORT = process.env.PORT||8050
app.listen(PORT);
/*app.listen(PORT,()=>{
    console.log(`Server is running on http://localhost:${PORT}`);
})*/

app.use(morgan('tiny'));

connectDB();


app.use(bodyparser.urlencoded({ extended: false }))
app.use(bodyparser.json())

app.set('view engine', 'ejs')

app.use(express.static('assets'))

app.use('/css', express.static(path.resolve(__dirname,"assets/css")))
app.use('/img', express.static(path.resolve(__dirname,"assets/img")))
app.use('/js', express.static(path.resolve(__dirname,"assets/js")))

//set route path
app.use('/',require('./routes/router'))


